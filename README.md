|      | __BADGE__ |
|:-------:|:-------:|
|__WINDOWS__|[![badge](https://www.dropbox.com/s/11lskz2c58glea9/windows.svg?raw=1)](https://gitlab.com/mombassa/pythonfile)|
|__LINUX__|[![badge](https://www.dropbox.com/s/vb81osv4ka4l0pe/ubuntu.svg?raw=1)]()|
|__macOS__|[![badge](https://www.dropbox.com/s/p76rcs85pfc6w42/macOS.svg?raw=1)]()|
